package ru.gns34.easy.support.domain;

/**
 * TaskExecutor
 *
 * @since 0.0.1 (08.05.2018)
 * @author Nikita 
 * 
 */
@FunctionalInterface
public interface ExceptionExecutor {
    void execute(Exception exception);
}
