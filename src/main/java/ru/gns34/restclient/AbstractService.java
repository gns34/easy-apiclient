package ru.gns34.restclient;

import java.util.Collection;

import flexjson.JSONDeserializer;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import ru.gns34.easy.support.exception.AppException;
import ru.gns34.easy.support.util.UtilJson;
import ru.gns34.easy.support.util.json.CalendarTransformer;
import ru.gns34.easy.support.util.json.SqlDateTransformer;
import ru.gns34.easy.support.util.json.SqlTimestampTransformer;

/**
 * AbstractService
 *
 * @since version(31-05-2018)
 * @author Гончаров Никита 
 * 
 */
public abstract class AbstractService {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private final OkHttpClient okHttpClient;
    private final String serverURL;
    
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    public AbstractService(OkHttpClient okHttpClient, String serverURL) {
        this.okHttpClient = okHttpClient;
        this.serverURL = serverURL;
    }
    // ~ Методы =============================================================================================
    protected Request.Builder buildRequest() {
        Request.Builder request = new Request.Builder();
        return request;
    }
    
    protected ResponseBody callServer(String methodURL) throws AppException {
        return callServer("GET", methodURL, null);
    }
    
    protected ResponseBody callServer(String methodURL, Object input) throws AppException {
        return callServer("POST", methodURL, input);
    }
    
    private ResponseBody callServer(String method, String methodURL, Object input) throws AppException {
        RequestBody requestBody = null;
        if (input != null) {
            final String json = UtilJson.ToJSONString(input);
            requestBody = RequestBody.create(MediaType.parse("application/json"), json);
        }
        final String urlMethod = serverURL + methodURL;
        Request request = buildRequest()
                .method(method, requestBody)
                .url(urlMethod)
                .build();
        Response execute = null;
        try {
            execute = okHttpClient.newCall(request).execute();
        } catch (Exception e) {
            throw new AppException(AppException.CODE_INTERNAL_ERROR, "Сбой в сервисе http-клиента", e);
        }
        ResponseBody body = execute.body();
        if (!execute.isSuccessful()) {
            String cnt = null;
            try {
                cnt = body.string();
            } catch (Exception e) {
                // FIXME: Логируем ошибку
            }
            final String message = String.format("Ошибка[%s] сервера rest-клиента по запросу '%s'. Ответ: '%s'", execute.code(), urlMethod, cnt);
            throw new AppException(AppException.CODE_INTERNAL_ERROR, message);
        }
        return body;
    }
    
    protected String exctractBody(ResponseBody body) throws AppException {
        String json;
        try {
            json = body.string();
        } catch (Exception e) {
            throw new AppException(AppException.CODE_IO_ERROR, "Неудалось извлечь данные из ответа", e);
        }
        return json;
        
    }
    
    @SuppressWarnings("unchecked")
    protected <T, C extends Collection<T>> C ToJSONObjects(String json, Class<C> clazzCollection, Class<T> clazzParam) {
        JSONDeserializer<Collection<T>> deSerializer = new JSONDeserializer<Collection<T>>();
        deSerializer = deSerializer
                        .use("values", clazzParam)
                        .use(java.util.Calendar.class, new CalendarTransformer())
                        .use(java.sql.Date.class, new SqlDateTransformer("yyyy-MM-dd"))
                        .use(java.sql.Timestamp.class, new SqlTimestampTransformer("yyyy-MM-dd HH:mm:ss"));
        return (C) deSerializer.deserialize(json, clazzCollection);
    }
    
}
