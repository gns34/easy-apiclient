package ru.gns34.restclient;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.internal.Util;
import okio.ByteString;
import okhttp3.Response;

/**
 * AuthenticationPlainInterceptor
 *
 * @since version(24-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class AuthenticationPlainInterceptor implements Interceptor {

    // ~ Статические поля/инициализации =====================================================================
    private static final Charset CHARSET = Util.UTF_8;
    
    // ~ Переменные(свойства) класса ========================================================================
    private final String login;
    private final String password;
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    public AuthenticationPlainInterceptor(String login, String password) {
        this.login = ByteString.encodeString(login, CHARSET).base64();
        this.password = ByteString.encodeString(password, CHARSET).base64();
    }
    
    // ~ Методы =============================================================================================
    /* (non-Javadoc)
     * @see okhttp3.Interceptor#intercept(okhttp3.Interceptor.Chain)
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();
        final Builder modify = request.newBuilder()
                .header("login", this.login)
                .header("password", this.password);
        return chain.proceed(modify.build());
    }
    
    // ~ Доступ к свойствам =================================================================================
    
}
