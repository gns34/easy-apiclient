package ru.gns34.restclient;

import ru.gns34.easy.support.exception.AppException;

/**
 * ZstoreException
 *
 * @since version(18-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class RestException extends AppException {

    // ~ Статические поля/инициализации =====================================================================
    private static final long serialVersionUID = 1L;
    
    // ~ Переменные(свойства) класса ========================================================================
    
    // ~ Управляемые объекты(Beans) =========================================================================

    // ~ Конструктор ========================================================================================
    public RestException(AppException ex) {
        super(ex);
    }

    public RestException(Exception exception) {
        super(exception);
    }

    public RestException(String code, String message, boolean forUI) {
        super(code, message, forUI);
    }

    public RestException(String code, String message, Throwable throwable, boolean forUI) {
        super(code, message, throwable, forUI);
    }

    public RestException(String code, String message, Throwable throwable) {
        super(code, message, throwable);
    }

    public RestException(String code, String message) {
        super(code, message);
    }

    public RestException(String message) {
        super(message);
    }

    // ~ Методы =============================================================================================
    
    // ~ Доступ к свойствам =================================================================================
    
}
