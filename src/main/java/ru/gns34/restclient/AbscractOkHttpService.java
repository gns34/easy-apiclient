package ru.gns34.restclient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import flexjson.JSONDeserializer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import ru.gns34.easy.support.util.UtilJson;
import ru.gns34.easy.support.util.UtilString;
import ru.gns34.easy.support.util.json.CalendarTransformer;
import ru.gns34.easy.support.util.json.SqlDateTransformer;
import ru.gns34.easy.support.util.json.SqlTimestampTransformer;

/**
 * AbscractOkHttpService
 *
 * @since 1.0 (31-05-2018)
 * @author Гончаров Никита 
 * 
 */
public abstract class AbscractOkHttpService {
    
    //~ Статические поля/инициализации ==========================================================================
    private static final String USER_AGENT = "Rest client";
    private static final String USER_AGENT_HEADER = "User-Agent";
    
    private static byte[] getBytesFromInputStream(InputStream is) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream(); 
        byte[] buffer = new byte[0xFFFF];
        for (int len = is.read(buffer); len != -1; len = is.read(buffer)) { 
            os.write(buffer, 0, len);
        }
        return os.toByteArray();
    }
    
    //~ Переменные(свойства) класса =============================================================================
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private final OkHttpContainer container;
    
    //~ Управляемые объекты(Beans) ==============================================================================
    
    //~ Конструктор =============================================================================================
    public AbscractOkHttpService(OkHttpContainer container) {
        this.container = container;
    }
    
    protected abstract String getControllerSuffix();
    
    //~ Методы ==================================================================================================
    protected String exctractBody(ResponseBody body) throws RestException {
        String json;
        try {
            json = body.string();
        } catch (Exception e) {
            throw new RestException(RestException.CODE_IO_ERROR, "Неудалось извлечь данные из ответа", e);
        }
        logger.debug("Получили ответ - {}", json);
        System.out.println("Получили ответ - {}" + json);
        return json;
        
    }
    
    protected <T> T extractObject(final ResponseBody body, Class<T> clazz) throws RestException {
        return extractObject(exctractBody(body), clazz);
    }
    
    protected <T> T extractObject(String json, Class<T> clazz) throws RestException {
        if (UtilString.isBlank(json)) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, String.format("Сбой обработки неожиданно получили пустой ответ"));
        }
        try {
            final JSONDeserializer<T> deSerializer = new JSONDeserializer<T>();
            return useDeSerializer(deSerializer
                            .use(java.util.Calendar.class, new CalendarTransformer())
                            .use(java.sql.Date.class, new SqlDateTransformer("yyyy-MM-dd"))
                            .use(java.sql.Timestamp.class, new SqlTimestampTransformer("yyyy-MM-dd HH:mm:ss")))
                            .deserialize(json, clazz);
        } catch (Exception e) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, String.format("Сбой обработки ответа '%s'", json), e);
        }
    }
    
    protected <T> JSONDeserializer<T> useDeSerializer(JSONDeserializer<T> use) {
        return use;
    }

    protected InputStream extractStream(ResponseBody body) {
        return body.byteStream();
    }
    
    protected <T, C extends Collection<T>> C extractObjects(final ResponseBody body, Class<C> clazzCollection, Class<T> clazzParam) throws RestException {
        return extractObjects(exctractBody(body), clazzCollection, clazzParam);
    }
    
    @SuppressWarnings("unchecked")
    protected <T, C extends Collection<T>> C extractObjects(String json, Class<C> clazzCollection, Class<T> clazzParam) throws RestException {
        if (UtilString.isBlank(json)) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, String.format("Сбой обработки неожиданно получили пустой ответ"));
        }
        try {
            JSONDeserializer<Collection<T>> deSerializer = new JSONDeserializer<Collection<T>>();
            deSerializer = useDeSerializer(deSerializer
                            .use("values", clazzParam)
                            .use(java.util.Calendar.class, new CalendarTransformer())
                            .use(java.sql.Date.class, new SqlDateTransformer("yyyy-MM-dd"))
                            .use(java.sql.Timestamp.class, new SqlTimestampTransformer("yyyy-MM-dd HH:mm:ss")));
            C deserialize = (C) deSerializer.deserialize(json, clazzCollection);
            if (deserialize == null) {
                return clazzCollection.newInstance();
            }
            return deserialize;
        } catch (Exception e) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, "Сбой обработки ответа", e);
        }
    }
    
    protected ResponseBody callServer(final String methodPath) throws RestException {
        return callServerSimple("GET", methodPath, null);
    }
    
    protected ResponseBody callServer(final String methodPath, Map<String, String> input) throws RestException {
        return callServerAsPostForm(methodPath, input);
    }
    
    protected ResponseBody callServer(final String methodPath, Object input) throws RestException {
        return callServerSimple("POST", methodPath, input);
    }
    
    private ResponseBody callServerSimple(String method, final String methodPath, Object input) throws RestException {
        final Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.addHeader(USER_AGENT_HEADER, USER_AGENT);
        RequestBody requestBody = null;
        if (input != null) {
            final String json = UtilJson.ToJSONString(input);
            requestBody = RequestBody.create(MediaType.parse("application/json"), json);
        }
        try {
            requestBuilder.method(method, requestBody);
        } catch (Exception e) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, "Ошибка rest-клиента не правильно сформирован method-запроса", e);
        }
        setRequestUrl(requestBuilder, methodPath);        
        return executeRequest(requestBuilder);
    }
    
    private ResponseBody callServerAsPostForm(final String methodPath, Map<String, String> input) throws RestException {
        final Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.addHeader(USER_AGENT_HEADER, USER_AGENT);

        RequestBody requestBody = null;
        okhttp3.MultipartBody.Builder multipartForm = new MultipartBody.Builder().setType(MultipartBody.FORM);
        if (input != null) {
            for (String paramKey : input.keySet()) {
                multipartForm.addFormDataPart(paramKey, input.get(paramKey));
            }
            requestBody = multipartForm.build();
        }
        try {
            requestBuilder.post(requestBody);
        } catch (Exception e) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, "Ошибка rest-клиента не правильно сформирован method-запроса", e);
        }
        setRequestUrl(requestBuilder, methodPath);        
        return executeRequest(requestBuilder);
    }
    
    protected ResponseBody callServer(String methodPath, Object input, InputStream stream) throws RestException {
        final Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.addHeader(USER_AGENT_HEADER, USER_AGENT);
        
        RequestBody requestBody = null;
        okhttp3.MultipartBody.Builder multipartForm = new MultipartBody.Builder().setType(MultipartBody.FORM);
        
        if (input != null) {
            final String json = UtilJson.ToJSONString(input);
            multipartForm.addFormDataPart("model", "file.name",
                    RequestBody.create(MediaType.parse("application/json"), json.getBytes())
            );
        }
        try {
            multipartForm.addFormDataPart("attach", "file.name",
                    RequestBody.create(MediaType.parse("application/octet-stream"), getBytesFromInputStream(stream))
            );
        } catch (Exception e) {
            throw new RestException(RestException.CODE_IO_ERROR, "Ошибка формирвоания пакета данных для отправки файла");
        }
        if (input != null || stream != null) {
            requestBody = multipartForm.build();
        }
        
        try {
            requestBuilder.post(requestBody);
        } catch (Exception e) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, "Ошибка rest-клиента не правильно сформирован method-запроса", e);
        }
        
        setRequestUrl(requestBuilder, methodPath);        
        return executeRequest(requestBuilder);
    }

    private ResponseBody executeRequest(final Request.Builder requestBuilder) throws RestException {
        Request request = requestBuilder.build();
        Response execute = null;
        bodyToString(request);
        try {
            execute = this.container.getOkHttpClient().newCall(request).execute();
        } catch (Exception e) {
            throw new RestException(RestException.CODE_INTERNAL_ERROR, "Сбой в сервисе http-клиента", e);
        }
        ResponseBody body = execute.body();
        if (!execute.isSuccessful()) {
            String cnt = null;
            try {
                cnt = body.string();
            } catch (Exception e) {
                logger.warn("Ошибка вычитывания тела запроса из {}", body);
            }
            
            logger.info(execute.toString());
            
            String message = null;
            try {
                String exception = execute.header("exception");
                if (exception != null) {
                    message = new String(Base64.decodeBase64(exception.getBytes("UTF-8")));
                }
            } catch (UnsupportedEncodingException e) {
                logger.error(e.getMessage(), e);
            }
            
            if (message == null) {
                message = String.format("Ошибка[%s] сервера rest-клиента по запросу '%s'. Ответ: '%s'", execute.code(), request.url(), cnt);
            }
            
            throw new RestException(RestException.CODE_INTERNAL_ERROR, message);
        }
        return body;
    }

    private void setRequestUrl(Builder requestBuilder, String methodPath) throws RestException {
        final StringBuilder url = new StringBuilder(128);
        url.append(getRequestUrl(methodPath));
        try {
            requestBuilder.url(url.toString());
        } catch (Exception e) {
            final String message = String.format("Ошибка rest-клиента не правильно сформирован url-запроса '%s'", url.toString());
            throw new RestException(RestException.CODE_INTERNAL_ERROR, message, e);
        }
    }

    private String getRequestUrl(String methodPath) {
        StringBuilder url = new StringBuilder(this.container.getRequestUrl());
        url.append('/');
        url.append(getControllerSuffix());
        url.append('/');
        url.append(methodPath);
        return url.toString();
    }

    private void bodyToString(final Request request){
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            String reqStr = buffer.readUtf8();
            logger.debug("Отправляем запрос \n" + reqStr);
        } catch (final Exception e) {
            
        }
    }
    
}
