package ru.gns34.restclient;

import okhttp3.OkHttpClient;
import ru.gns34.easy.support.util.UtilString;

/**
 * OkHttpContainer
 *
 * @since 1.0 (31-05-2018)
 * @author Гончаров Никита 
 * 
 */
public abstract class OkHttpContainer {
    
    //~ Статические поля/инициализации ==========================================================================
    private final static String SERVICE_PREFIX = "service";
    
    //~ Переменные(свойства) класса =============================================================================
    private final OkHttpClient okHttpClient;
    private final String host;
    private final String serviceVersion;
    private final String requestUrl;
    
    //~ Управляемые объекты(Beans) ==============================================================================
    
    //~ Конструктор =============================================================================================
    protected OkHttpContainer(OkHttpClient okHttpClient, String host) {
        this(okHttpClient, host, UtilString.BLANK);
    }
    protected OkHttpContainer(OkHttpClient okHttpClient, String host, String serviceVersion) {
        this.okHttpClient = okHttpClient;
        this.host = host;
        this.serviceVersion = serviceVersion;
        StringBuilder url = new StringBuilder();
        url.append(host).append('/');
        if (UtilString.isNotBlank(serviceVersion)) {
            url.append(SERVICE_PREFIX).append('/').
            append(serviceVersion);
        }
        this.requestUrl = url.toString();
    }
    
    //~ Методы ==================================================================================================
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Контейнер [" + host + "#" + hashCode() + "]";
    }
    
    //~ Доступ к свойствам ======================================================================================
    /**
     * @return okHttpClient
     */
    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    /**
     * @return requestUrl
     */
    public String getRequestUrl() {
        return requestUrl;
    }

    /**
     * @return host
     */
    public String getHost() {
        return host;
    }

    /**
     * @return serviceVersion
     */
    public String getServiceVersion() {
        return serviceVersion;
    }
    
}
