package ru.ztech.zstore;

import android.os.AsyncTask;
import ru.gns34.easy.support.domain.ExceptionExecutor;
import ru.gns34.easy.support.domain.TaskExecutor;

/**
 * ZStoreAndroidAsyncTask
 *
 * @since 0.0.1 (02-07-2018)
 * @author Гончаров Никита 
 * 
 */
public abstract class AndroidAsyncTask<T> extends AsyncTask<T, Void, Void> {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private final TaskExecutor<T> response;
    private final ExceptionExecutor exception;
    private T result;
    private Exception exc;
    
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    public AndroidAsyncTask(TaskExecutor<T> response, ExceptionExecutor exception) {
        this.response = response;
        this.exception = exception;
    }
    
    // ~ Методы =============================================================================================
    protected abstract T callService() throws Exception;
    
    @SuppressWarnings("unchecked")
    public void run() {
        try {
            execute();
        } catch (Exception e) {
            exception.execute(e);
        }
    }
    
    @Override
    protected Void doInBackground(@SuppressWarnings("unchecked") T... params) {
        try {
            // Вызываемый метод
            this.result = callService();
        } catch (Exception e) {
            this.exc = e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (this.exc != null) {
            exception.execute(exc);
        } else {
            response.execute(this.result);
        }
    }

}
