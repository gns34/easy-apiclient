package ru.ztech.zstore.client.data;

/**
 * ChangePasswordRq
 *
 * @since version(29-06-2018)
 * @author Гончаров Никита 
 * 
 */
public class ChangePasswordRq {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private final String passwordNew; 
    private final String passwordOld;
    
    // ~ Конструктор ========================================================================================
    public ChangePasswordRq(String passwordNew, String passwordOld) {
        this.passwordOld = passwordOld;
        this.passwordNew = passwordNew;
    }
    // ~ Методы =============================================================================================

    // ~ Доступ к свойствам =================================================================================
    /**
     * @return passwordNew
     */
    public String getPasswordNew() {
        return passwordNew;
    }

    /**
     * @return passwordOld
     */
    public String getPasswordOld() {
        return passwordOld;
    }
    
    
}
