package ru.ztech.zstore.model;

/**
 * Credentials
 *
 * @since version(09-03-2019)
 * @author Гаевский Никита 
 * 
 */
public class Credentials {
    private boolean isTestEnvironment = false;
    private com.burgstaller.okhttp.digest.Credentials credentials;
    //~ Статические поля/инициализации ==========================================================================
    /**
     * @return isTestEnvironment
     */
    public boolean isTestEnvironment() {
        return isTestEnvironment;
    }
    /**
     * @param isTestEnvironment - Установить isTestEnvironment
     */
    public void setTestEnvironment(boolean isTestEnvironment) {
        this.isTestEnvironment = isTestEnvironment;
    }
    /**
     * @return credentials
     */
    public com.burgstaller.okhttp.digest.Credentials getCredentials() {
        return credentials;
    }
    /**
     * @param credentials - Установить credentials
     */
    public void setCredentials(com.burgstaller.okhttp.digest.Credentials credentials) {
        this.credentials = credentials;
    }
}
